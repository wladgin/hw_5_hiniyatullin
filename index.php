<?php

require 'exchange_rates.php';
$tovar = [
    [
        'title' => 'Бананы',
        'price_type' => 'uah',
        'price_val' => 30,
        'discount_type' => 'value',
        'discount_val' => 4
    ],
    [
        'title' => 'Авокадо',
        'price_type' => 'usd',
        'price_val' => 2.98,
        'discount_type' => 'percent',
        'discount_val' => 2
    ],
    [
        'title' => 'Лимон',
        'price_type' => 'eur',
        'price_val' => 1.59,
        'discount_type' => 'value',
        'discount_val' => 0.15
    ],
    [
        'title' => 'Яблоки',
        'price_type' => 'uah',
        'price_val' => 23,
        'discount_type' => 'percent',
        'discount_val' => 5
    ],
    [
        'title' => 'Мандарины',
        'price_type' => 'usd',
        'price_val' => 1.60,
        'discount_type' => 'value',
        'discount_val' => 0.12
    ],
    [
        'title' => 'Хурма',
        'price_type' => 'eur',
        'price_val' => 1.86,
        'discount_type' => 'percent',
        'discount_val' => 4
    ],
    [
        'title' => 'Сливы',
        'price_type' => 'uah',
        'price_val' => 35,
        'discount_type' => 'value',
        'discount_val' => 7
    ],
    [
        'title' => 'Виноград',
        'price_type' => 'usd',
        'price_val' => 1.7,
        'discount_type' => 'percent',
        'discount_val' => 7
    ],
    [
        'title' => 'Клублинка',
        'price_type' => 'uah',
        'price_val' => 50,
        'discount_type' => 'value',
        'discount_val' => 13
    ],
    [
        'title' => 'Голубика',
        'price_type' => 'eur',
        'price_val' => 3.81,
        'discount_type' => 'percent',
        'discount_val' => 7
    ],
];


function get_price($tovar) // функция подсчета скидки
{
    //если скидки нет или ввели скидку <0
    if ((float)$tovar['discount_val'] <= 0) {
        return $tovar['price_val'];
    }

    $discount = $tovar['discount_val'];

    if ($tovar['discount_type'] === 'percent') {
        $discount = round($tovar['price_val'] / 100 * $tovar['discount_val'], 2);
    }
    return $tovar['price_val'] - $discount;
}


function get_currency($tovar) // функция перевода окончательной цены, с учетом скидок, в грн
{
    global $currency;
    return round(get_price($tovar) * $currency[$tovar['price_type']]['course'], 2);
}

function get_valuta($tovar) // функция перевода цены, без  учета скидок, в грн
{
    global $currency;
    return round($tovar['price_val'] * $currency[$tovar['price_type']]['course'], 2);
}

echo "<h1 align='center'>Стоимость товаров</h1>>";

foreach ($tovar as $tovars) {
    echo 'Товар: ' . $tovars['title'] . '<br>' .
        'Стоимость без скидки = ' . get_valuta($tovars) . ' грн' . '<br>' .
        'Окончательная стоимость с учетом скидки = ' . get_currency($tovars) . ' грн' . '<br>' . '<p>';
}